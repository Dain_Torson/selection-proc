import math
import xlwt


def write_to_col(sheet, data, col, label=""):

    sheet.write(0, col, label)
    for idx in range(len(data)):
        sheet.write(idx + 1, col, data[idx])


def create_eq_int_hist_data(selection):
    """
    Generates histogram data
    :param selection:
    :return:
    """
    h = (selection[-1] - selection[0]) / math.sqrt(len(selection))

    intervals = []
    interval = []
    borders = []
    value = selection[0] + h
    prev_value = selection[0]
    borders.append(selection[0])
    borders.append(value)
    idx = 0

    while True:
        if selection[idx] <= value:
            if selection[idx] >= prev_value:
                interval.append(selection[idx])
                idx += 1
        else:
            intervals.append(interval)
            prev_value = value
            value += h
            if value < selection[-1]:
                borders.append(value)
                interval = []
            else:
                intervals[-1].append(selection[-1])
                break

    num_of_entries = [len(x) for x in intervals]
    probability = [x / len(selection) for x in num_of_entries]
    funcs = [x / h for x in probability]

    print("Interval length: " + str(h))
    print("Check: " + str(len(borders)) + " should be " + str(math.sqrt(len(selection))) + " + 1")
    # print(borders)
    # print(num_of_entries)
    print("Check: " + str(sum(probability)) + " should be 1.0")
    # print(probability)
    # print(funcs)

    book = xlwt.Workbook();
    sheet = book.add_sheet("Analyse result")

    a = borders[:-1]
    b = borders[1:]

    write_to_col(sheet, a, 0, "Aj")
    write_to_col(sheet, b, 1, "Bj")
    write_to_col(sheet, [h], 2, "hj")
    write_to_col(sheet, num_of_entries, 3, "kj")
    write_to_col(sheet, probability, 4, "pj")
    write_to_col(sheet, funcs, 5, "fj")
    write_to_col(sheet, borders, 6, "borders")

    book.save("results/hist1data.xls")


def create_eq_prob_hist_data(selection):
    """
    Generates histogram data
    :param selection:
    :return:
    """

    num_of_entries = len(selection) / math.sqrt(len(selection))
    probability = 1.0 / num_of_entries

    intervals = []
    interval = []
    borders = []
    h = []
    borders.append(selection[0])

    for value in selection:
        interval.append(value)
        if len(interval) == num_of_entries:
            intervals.append(interval)
            interval = []

    for idx in range(len(intervals)):
        if idx + 1 < len(intervals):
            borders.append((intervals[idx + 1][0] + intervals[idx][-1]) / 2)

    borders.append(selection[-1])

    for idx in range(len(borders)):
        if idx + 1 < len(borders):
            h.append(borders[idx + 1] - borders[idx])

    funcs = [probability / x for x in h]

    book = xlwt.Workbook()
    sheet = book.add_sheet("Analyse result")

    a = borders[:-1]
    b = borders[1:]

    write_to_col(sheet, a, 0, "Aj")
    write_to_col(sheet, b, 1, "Bj")
    write_to_col(sheet, h, 2, "hj")
    write_to_col(sheet, [num_of_entries], 3, "kj")
    write_to_col(sheet, [probability], 4, "pj")
    write_to_col(sheet, funcs, 5, "fj")
    write_to_col(sheet, borders, 6, "borders")

    book.save("results/hist2data.xls")




