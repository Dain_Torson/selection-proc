import simpleplot
import selection_proc as sp
import math

selection = [-1.22, -4.3, -8.49, -5.21, -8.31, -7.52, -2.61, -5.76, -2.83, -6.24,
 -8.51, -1.72, -0.14, -3.71, -2.22, -0.36, -5.84, -5.14, -0.49, -6.4, -2.06, -7.28,
  -0.43, -5.79, -8.28, -1.4, -3.57, -2.94, -6.62, -0.58, -7.13, -5.41, -7.32, -5.69,
  -6.04, -2.37, -5.63, -2.43, -4.14, -6.43, -7.44, -2.31, -5.47, -3.79, -7.64, -1.34,
  -5.32, -6.7, -8.64, -2.21, -7.61, -5.84, -1.36, -3.82, -7.06, -3.86, -0.72, -5.26,
  -1.13, -0.59, -2.05, -0.86, -0.23, -3.9, -6.16, -0.78, -2.59, -1.13, -1, -6.56,
  -0.94, -3.52, -0.41, -5.92,  -0.12, -8.65, -6.58, -6.4, -5.76, -6.19, -0.89, -5.02,
  -1.62, -4.69, -5.52, -3.4, -8.59, -3.46, -7.63, -4.17, -7.35, -2.85, -7.14, -8.13,
  -1.86, -7.07, -1.18, -3.72, -1.34, -5.66]

selection.sort()

y_vec = list(range(0, 100))

# data = simpleplot.PlotData(selection, y_vec)
# simpleplot.create_plot(data, "plot")

print("Length of selection " + str(len(selection)))
print(selection)

sp.create_eq_int_hist_data(selection)
sp.create_eq_prob_hist_data(selection)

n = len(selection)
s_sum = sum(selection)
expectation = s_sum / n
print("Sum: " + str(s_sum))
print("Expectation: " + str(expectation))

sq_selection = [x*x for x in selection]
sq_sum = sum(sq_selection)
dispersion = (sq_sum - n*expectation*expectation) / (n-1)

print(sq_selection)

print("Square Sum: " + str(sq_sum))
print("Dispersion: " + str(dispersion))

