# requires scipy, six, pyparsing, python-dateutil, matplotlib

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import spline


class PlotData:
    """
    Data for plotting
    """

    def __init__(self, x_vec, y_vec):
        self.x_vec = x_vec
        self.y_vec = y_vec
        self.title = "New Plot"
        self.x_label = "x"
        self.y_label = "y"
        self.grid = True


def create_plot(data, name, smooth=False):
    """
    Creates plot from PlotData object
    :param data: PlotData object
    :param name: output filename
    :param smooth: whether graph should be smoothed (could affect accuracy)
    :return:
    """

    x = np.array(data.x_vec)
    y = np.array(data.y_vec)

    if smooth:
        x_smooth = np.linspace(x.min(), x.max(), 300)
        y_smooth = spline(x, y, x_smooth)

        x = x_smooth
        y = y_smooth

    plt.plot(x, y)
    plt.title(data.title)
    plt.xlabel(data.x_label)
    plt.ylabel(data.y_label)
    plt.ylim(ymin=0)
    plt.xlim(xmin=-9)
    plt.grid(data.grid)

    plt.savefig("plots/" + name + ".png")
    plt.close()
